<?php

// set error reporting level
if (version_compare(phpversion(), '5.3.0', '>=') == 1)
  error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
else
  error_reporting(E_ALL & ~E_NOTICE);

// http://code.google.com/apis/youtube/dashboard/gwt/index.html
$sDevKey = 'AI39si7qxGmMtnRnGU6NBfzR4W7oursacyzXTo5SO1j0SRq7-q5CgBA1sXkxNyANYfNAvJjYMFUUPl0-JgxMRMNCYwYrWohhXA';

$sWebPageUrl = 'http://sellerwrxabhi.net63.net/index.php';
$sAuthUrl = urlencode($sWebPageUrl . '?action=auth');
$sOAuthUrl = "https://www.google.com/accounts/AuthSubRequest?next={$sAuthUrl}&scope=http%3A%2F%2Fgdata.youtube.com&session=0&secure=0";

// variables
$sToken = $sUploadUrl = $sUploadToken = $sNextUrl = $sVideoId = '';
$iUploadStatus = 0;

// actions
$sAction = $_REQUEST['action'];
switch ($sAction) {
    case 'auth': // step 1 - get access token and prepare form for sending to youtube with details of video
        $sToken = $_GET['token'];
        break;
    case 'prepare': // step 2 - send video details to youtube in order to obtain upload token and url

        // collect POSTed video details
        $sTitle = strip_tags($_POST['title']);
        $sDesc = strip_tags($_POST['description']);
        $sCategory = strip_tags($_POST['category']);
        $sKeywords = strip_tags($_POST['keywords']);
        $sToken = strip_tags($_POST['token']);

        // prepare data for youtube
        $sData = <<<EOF
<?xml version="1.0"?>
<entry xmlns="http://www.w3.org/2005/Atom"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:yt="http://gdata.youtube.com/schemas/2007">
  <media:group>
    <media:title type="plain">{$sTitle}</media:title>
    <media:description type="plain">{$sDesc}</media:description>
    <media:category scheme="http://gdata.youtube.com/schemas/2007/categories.cat">{$sCategory}</media:category>
    <media:keywords>{$sKeywords}</media:keywords>
  </media:group>
</entry>
EOF;

        $sUplTokenUrl = "http://gdata.youtube.com/action/GetUploadToken";
        $aHeaders = array("POST /action/GetUploadToken HTTP/1.1",
            "Host: gdata.youtube.com",
            "Authorization: AuthSub token=" . $sToken,
            "X-GData-Key: key=" . $sDevKey,
            "Content-Length: " . strlen($sData),
            "Content-Type: application/atom+xml; charset=UTF-8"
        );
        $sUserAgent = $_SERVER['HTTP_USER_AGENT']; 

        // send request to youtube
        $oCurl = curl_init();
        curl_setopt($oCurl, CURLOPT_URL, $sUplTokenUrl);
        curl_setopt($oCurl, CURLOPT_HTTPHEADER, $aHeaders);
        curl_setopt($oCurl, CURLOPT_HEADER, true);
        curl_setopt($oCurl, CURLOPT_TIMEOUT, 30);
        curl_setopt($oCurl, CURLOPT_USERAGENT, $sUserAgent); 
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($oCurl, CURLOPT_POSTFIELDS, $sData); 
        curl_setopt($oCurl, CURLOPT_POST, true);
        $sXmlRes = curl_exec($oCurl);
        curl_close($oCurl);

        // lets find xml result and parse it in order to get upload url and token
        $sXmlSrc = substr($sXmlRes, strpos($sXmlRes, '<?xml'));
        if ($sXmlSrc != '') {
            $oXml = simplexml_load_string($sXmlSrc);
            $sUploadUrl = $oXml->url;
            $sUploadToken = $oXml->token;

            $sNextUrl = urlencode($sWebPageUrl . '?action=finish');
        }
        break;

    case 'finish': // step 3 - final. Our video has just posted to youtube, display results
        $iUploadStatus = (int)$_GET['status'];
        $sVideoId = $_GET['id'];
        break;
}

?>
<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="utf-8" />
        <title>Uploader Module For SellerWorx</title>
        <link href="main.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <header>
            <h2>YouTube Uploader Module for SellerWorx</h2>
            <a href="http://www.sellerworx.com/" class="stuts">Visit SellerWorx<span>SellerWorx</span></a>
        </header>
        <img src="ytb.png" class="ytb" alt="youtube" />

    <?php if (! $sToken && $sAction != 'finish'): ?>
        <center>
        <h1>Step 1.Authorize</h1>
        <h2>Please click <a href="<?= $sOAuthUrl ?>">this link</a> in order to authorize your account at youtube</h2>
        </center>
    <?php endif ?>

    <?php if ($sToken && !$sUploadToken): ?>
        <center>
        <h1>Step 2. Video info</h1>
        <h2>Enter all the details about your Video</h2>
        </center>
        <form method="post" action="index.php">
            <input type="hidden" name="token" value="<?php echo($sToken) ?>" />
            <input type="hidden" name="action" value="prepare" />
            <label for="title">Name of Product:</label>
            <input type="text" name="title" value="Sample title of your video" />
            <label for="description">Description of Product:</label>
            <input type="text" name="description" value="Sample description of your video" />
            <label for="category">Category of Product:</label>
            <select name="category">
                <option value="Autos">Autos &amp; Books</option>
                <option value="Comedy">Clothing</option>
                <option value="Education">Electronics</option>
                <option value="Entertainment">FootWear</option>
                <option value="Film">Film &amp; Animation</option>
                <option value="Games">Games</option>
                <option value="Howto"> Households&amp;Utensils</option>
                
            </select>
            <label for="keywords">Keywords:</label>
            <input type="text" name="keywords" value="Keywords" />
            <input type="submit" name="submit" value="Continue" />
        </form>
    <?php endif ?>

    <?php if ($sUploadUrl==true && $sUploadToken==true && $sNextUrl==true): ?> 
        <center>
        <h1>Step 3. Upload Video</h1>
        <h2>Select video and start upload</h2>
        </center>
        <form method="post" enctype="multipart/form-data" action="<?php echo $sUploadUrl . '?nexturl=' . $sNextUrl ?>">
            <input type="hidden" name="token" value="<?php echo($sUploadToken) ?>" />
            <label for="file">Select Video to upload:</label>
            <input type="file" name="file" size="41" />
            <input type="submit" name="submit" value="Start upload" />
        </form>
    <?php endif ?>

    <?php if ($iUploadStatus == 200 && $sVideoId): ?>
        <center>
        <h1>Step 4. Final</h1>
        <h2>Your Product's video has just uploaded to youtube and available <a href="http://www.youtube.com/watch?v=<?php echo($sVideoId) ?>" target="_blank">here</a></h2>
        </center>
    <?php elseif ($iUploadStatus): ?>
        <center>
        <h1>Step 4. Final</h1>
        <h2>Upload is failed. Error #<?php echo($iUploadStatus) ?></h2>
        </center>
    <?php endif ?>

    <?php if ($sToken || $sVideoId): ?>
        <br />
        <center><h2>(<a href="<?php echo $sWebPageUrl ?>">Click here to start again</a>)</h2></center>
    <?php endif ?>

</body>
</html>